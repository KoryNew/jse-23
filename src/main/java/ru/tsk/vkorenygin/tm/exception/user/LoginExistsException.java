package ru.tsk.vkorenygin.tm.exception.user;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException(String login) {
        super("Error! Login '" + login + "' already exists.");
    }

}

package ru.tsk.vkorenygin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void show(@NotNull Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project id: " + task.getProjectId());
        System.out.println("Create date: " + task.getCreateDate());
        System.out.println("Start date: " + task.getStartDate());
    }

}

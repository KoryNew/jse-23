package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.api.service.IService;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.entity.EntityNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public boolean existsById(final @Nullable String id) {
        if (DataUtil.isEmpty(id))
            return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (DataUtil.isEmpty(index))
            return false;
        return repository.existsByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public @NotNull List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public @NotNull List<E> findAll(final @Nullable Comparator<E> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public @NotNull Optional<E> findByIndex(final @Nullable Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > repository.getSize())
            throw new EntityNotFoundException();
        return repository.findByIndex(index);
    }

    @Override
    public @NotNull E add(final @Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void addAll(final @Nullable Collection<E> collection) {
        if (collection == null) throw new EntityNotFoundException();
        repository.addAll(collection);
    }

    @Override
    public @NotNull Optional<E> findById(final @Nullable String id) {
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public @NotNull Optional<E> removeById(final @Nullable String id) {
        return repository.removeById(id);
    }

    @Override
    public @NotNull Optional<E> removeByIndex(final @NotNull Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        final Optional<E> entity = repository.findByIndex(index);
        if (!entity.isPresent())
            throw new EntityNotFoundException();
        return repository.removeByIndex(index);
    }

    @Override
    public @NotNull Optional<E> remove(final @NotNull E entity) {
        return repository.remove(entity);
    }

}

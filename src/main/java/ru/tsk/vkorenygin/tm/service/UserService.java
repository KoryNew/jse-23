package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.api.service.IUserService;
import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyEmailException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyLoginException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyPasswordException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.exception.user.EmailOccupiedException;
import ru.tsk.vkorenygin.tm.exception.user.LoginExistsException;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import ru.tsk.vkorenygin.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public boolean isLoginExists(final @Nullable String login) throws AbstractException {
        if (DataUtil.isEmpty(login)) return false;
        return (findByLogin(login).isPresent());
    }

    @Override
    public boolean isEmailExists(final @Nullable String email) throws AbstractException {
        if (DataUtil.isEmpty(email)) return false;
        return (findByEmail(email).isPresent());
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password, final @Nullable String email) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email))
            throw new EmptyEmailException();
        if (findByLogin(login).isPresent())
            throw new LoginExistsException(login);
        if (findByEmail(email).isPresent())
            throw new EmailOccupiedException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public @NotNull User create(final @Nullable String login, final @Nullable String password, final @Nullable Role role) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (findByLogin(login).isPresent())
            throw new LoginExistsException(login);
        if (role == null)
            throw new EmptyEmailException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public @NotNull Optional<User> findByLogin(final @Nullable String login) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public @NotNull Optional<User> findByEmail(final @Nullable String email) throws AbstractException {
        if (DataUtil.isEmpty(email))
            throw new EmptyEmailException();
        return userRepository.findByLogin(email);
    }

    @Override
    public @NotNull Optional<User> removeByLogin(final @Nullable String login) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public @NotNull User setPassword(final @Nullable String userId, final @Nullable String password) throws AbstractException {
        if (DataUtil.isEmpty(userId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        user.ifPresent(e -> e.setPasswordHash(hash));
        return user.get();
    }

    @Override
    public @NotNull User updateUser(
            final @Nullable String userId,
            final @Nullable String firstName,
            final @Nullable String lastName,
            final @Nullable String middleName
    ) throws AbstractException {
        if (DataUtil.isEmpty(userId))
            throw new EmptyIdException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new AccessDeniedException();
        user.ifPresent(e -> {
            e.setFirstName(firstName);
            e.setLastName(lastName);
            e.setMiddleName(middleName);
        });
        return user.get();
    }

    @Override
    public @NotNull Optional<User> lockByLogin(@Nullable String login) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        final Optional<User> user = findByLogin(login);
        if(!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(true);
        return user;
    }

    @Override
    public @NotNull Optional<User> unlockByLogin(@Nullable String login) throws AbstractException {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        final Optional<User> user = findByLogin(login);
        if(!user.isPresent()) throw new AccessDeniedException();
        user.get().setLocked(false);
        return user;
    }

}

package ru.tsk.vkorenygin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;

import java.util.*;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();

    protected Predicate<E> hasId(String id) {
        return e -> id.equals(e.getId());
    }

    @Override
    public @NotNull E add(final @NotNull E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void addAll(final @NotNull Collection<E> collection) {
        collection.forEach(this::add);
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        return entities.stream()
                .anyMatch(hasId(id));
    }

    @Override
    public boolean existsByIndex(final int index) {
        return index < entities.size();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public @NotNull List<E> findAll() {
        return entities;
    }

    public @NotNull List<E> findAll(final @NotNull Comparator<E> comparator) {
        final List<E> entitiesSorted = new ArrayList<>(entities);
        entitiesSorted.sort(comparator);
        return entitiesSorted;
    }

    @Override
    public @NotNull Optional<E> findById(final @NotNull String id) {
        return entities.stream()
                .filter(hasId(id))
                .findFirst();
    }

    @Override
    public @NotNull Optional<E> findByIndex(final @NotNull Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public @NotNull Optional<E> removeById(final @NotNull String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Override
    public @NotNull Optional<E> removeByIndex(final @NotNull Integer index) {
        @Nullable final Optional<E> entity = findByIndex(index);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Override
    public @NotNull Optional<E> remove(final @NotNull E entity) {
        entities.remove(entity);
        return Optional.ofNullable(entity);
    }

}
